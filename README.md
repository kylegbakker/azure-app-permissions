# Azure App Permissions

PowerShell script that will audit and log Azure application permissions.

# Usage

1. Extract zip to folder

2. Run powershell as Administrator

3. `Install-Module AzureAD`

4. `Install-Module Az`

5. Close powershell and reopen as Administrator

6. PowerShell.exe -ExecutionPolicy Bypass -File "PATH-TO-SCRIPT\AzureAppPerms.ps1"

7. A login box will pop-up, enter your Azure admin credentials and click Ok. 

After the script has run, this will create a 'log.txt' in the script root folder. 
